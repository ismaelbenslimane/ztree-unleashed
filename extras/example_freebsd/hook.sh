#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

# To use this hook script, you basically have to do the following:
#
# 1. Set up the "Thin" server running FreeBSD. (Be sure to enable and start sshd,
#    but you should prohibit password logins in /etc/ssh/sshd_config.) What
#    follows are commands you have to execute on the "Thin" server as root (in bash).
#
# 2. Install WireGuard, nginx and sudo: `pkg install wireguard nginx-full sudo`
#
# 3. Install the generated WireGuard config into /usr/local/etc/wireguard/wg_zu.conf,
#    replace the PostUp and PostDown lines with
#
#      PostUp = sysctl net.inet.ip.forwarding=1
#      PostDown = sysctl net.inet.ip.forwarding=0
#
#    and start the VPN with `wg-quick up wg_zu` (has to be done on each reboot of "Thin" server or per crontab)
#
# 4. Install the nginx config from 'nginx_Thin.conf' into /usr/local/etc/nginx/nginx.conf.
#    Enable nginx in the rc.conf using `service nginx enable`. Start it with
#    `service nginx start`.
#
# 5. Add the following line to the sudoers file by executing `EDITOR=$(which nano) visudo`:
#
#      thin ALL=(ALL) NOPASSWD: /usr/sbin/service nginx reload
#
# 6. Execute `touch /usr/local/etc/nginx/zTu.conf` and `chown thin:thin /usr/local/etc/nginx/zTu.conf`.
# 
# 7. Put this server's ~/.ssh/id_ed25519.pub into /home/thin/.ssh/authorized_keys. ON THIS
#    SERVER (NOT THE THIN SERVER), execute: `ssh thin@$(cat /share/wg_thin_ip) mkdir -p .ssh`
#    and `scp ~/.ssh/id_ed25519.pub thin@$(cat /share/wg_thin_ip):/home/thin/.ssh/authorized_keys`
#
# 8. Verify that you can access the "Thin" server from this server:
#    `ssh thin@$(cat /share/wg_thin_ip) whoami`
#    should return 'thin'
#
# 9. Enable this script in ~/scripts/thin_hooks.sh by uncommenting it in line 7

THIN_IP=$(cat /share/wg_thin_ip)

scp /share/.session/nginx.conf thin@"$THIN_IP":/usr/local/etc/nginx/zTu.conf

ssh thin@"$THIN_IP" sudo service nginx reload 2> >(grep -v gethostbyname)
