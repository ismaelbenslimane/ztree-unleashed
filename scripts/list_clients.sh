#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

if [ ! -f ~/.zu/prefix ]; then
 	echo "File '~/.zu/prefix' not found. Error. Run ./init.sh first." 1>&2
 	exit 1
fi

getent passwd | awk -F: '{ print $1}' | grep ^$(cat ~/.zu/prefix)
