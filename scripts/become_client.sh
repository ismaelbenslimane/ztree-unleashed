#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

source read_settings.sh

echo "The following clients are available:$(tput sgr0)"
echo

OOUT=""
for i in `seq 0 $NCLIENTS`;
do
	OOUT=$OOUT" "$PREFIX$i
done

echo $OOUT | fold -s -w 60
echo
echo "$(tput setaf 6)Which client do you want to become?"
echo "$(tput sgr0)"
read user

sudo su - "$user"
