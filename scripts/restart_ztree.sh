#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

set -uo pipefail

source read_settings.sh

echo "$(tput setaf 6)Do you want to restart z-Tree? Type YES"
echo "$(tput setaf 6)if you do, and anything else if you don't."
echo "$(tput setaf 1)NOTE: You will lose data if you proceed."
echo "$(tput sgr0)"
read entry

if [[ $entry == "YES" ]]
then
	user=$PREFIX"0"
	
	sudo pkill --signal=INT -u $user wine
	sudo fuser -k 7000/tcp 2>/dev/null
	echo -n "Stopped z-Tree. Waiting 15 seconds. "
	sleep 15
	
	VDISP=$(grep -wE "$user" /share/.session/sessionfile | awk '{print $2}')
	SUBDIR=$(find /share/scratch -maxdepth 1 -type d -name '??????????????' | sort | tail -n 1)
	
	./start_ztree.sh $user $VDISP $(sudo cat /home/$user/.zu/core) $SUBDIR $(cat /share/.session/channel)
	echo "Started z-Tree."

else
	echo "Doing nothing."
	exit 1
fi
