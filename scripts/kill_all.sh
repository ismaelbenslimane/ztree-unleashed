#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

source read_settings.sh

echo "$(tput setaf 6)Do you want to terminate the session? Type YES"
echo "$(tput setaf 6)if you do, and anything else if you don't."
echo "$(tput setaf 1)NOTE: You will lose data if you proceed and if"
echo "$(tput setaf 1)you have not properly closed z-Tree. Only use"
echo "$(tput setaf 1)this at the end of a session!"
echo
echo "$(tput setaf 6)If you want to reuse the URLs given to subjects, use"
echo "$(tput setaf 6)'Reuse sessionfile'. This session's sessionfile was"
echo "$(tput setaf 3)"
find /share/scratch/??????????????/sessionfile | sort | tail -n 1
echo "$(tput sgr0)"
read entry

if [[ $entry == "YES" ]]
then
	pkill -INT -f websockify

	./list_clients.sh | while read line ; do sudo pkill --signal=INT -u $line ; done

	sudo fuser -k 7000/tcp 2>/dev/null
else
	echo "Doing nothing."
	exit 1
fi

if [[ $USE_THIN == 1 ]]; then
	./thin_hooks_stop.sh
fi
