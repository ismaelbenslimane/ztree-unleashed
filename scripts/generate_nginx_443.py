#!/usr/bin/env python3

from os.path import expanduser

with open(expanduser("~/.zu/zuserver"), 'r') as fp:
	zuserver = fp.read().strip()

with open(expanduser("~/.zu/ssl_cert"), 'r') as fp:
	ssl_cert = fp.read().strip()
	
with open(expanduser("~/.zu/ssl_key"), 'r') as fp:
	ssl_key = fp.read().strip()

with open(expanduser("~/.zu/host"), 'r') as fp:
	host = fp.read().strip()

with open("templates/nginx_443.conf", 'r') as fp:
	t_nginx = fp.read()

t_nginx = t_nginx.replace('ZUSERVER', zuserver).replace('SSL_CERT', ssl_cert).replace('SSL_KEY', ssl_key).replace('HOST', host)

print(t_nginx)
