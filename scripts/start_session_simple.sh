#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

set -o pipefail

source read_settings.sh

if sudo pgrep Xvfb > /dev/null; then
	echo "$(tput setaf 1)"
	echo "It seems like you are already running a session. I refuse to"
	echo "start a simultaneous session. Please \"Terminate session\" first."
	echo "$(tput sgr0)"

	if [ -n "$DISPLAY" ]
	then
		echo "Press enter to close this window."
		read
	fi
	
	exit 1
fi

echo "$(tput setaf 2)z-Tree unleashed v1.0"
echo "Start session"
echo
echo "$(tput setaf 6)Please select the $(tput bold)z-Leaf$(tput sgr0)$(tput setaf 6) executable. (The z-Tree"
echo "$(tput setaf 6)executable will be selected in the next step.)"
echo
./choose_exe.sh 2>/tmp/.zleaf

if [[ $? != 0 ]]; then
        if [ -n "$DISPLAY" ]
        then
		echo "$(tput sgr0)"
                echo "Press enter to close this window."
                read
        fi

	exit 1
fi

echo
echo "$(tput setaf 6)Please select the $(tput bold)z-Tree$(tput sgr0)$(tput setaf 6) executable."
./choose_exe.sh 2>/tmp/.ztree

rm -f /share/.session/zleaf.exe /share/.session/ztree.exe

ln -sf "$(cat /tmp/.zleaf)" /share/.session/zleaf.exe
ln -sf "$(cat /tmp/.ztree)" /share/.session/ztree.exe

if [ -z "$1" ]
then
	echo "$(tput setaf 6)How many clients do you wish to start?"
	echo "$(tput setaf 6)(Not counting the experimenter's client.)"
	echo "$(tput setaf 6)Press enter for the default ($NCLIENTS_START)."
	echo "$(tput setaf 6)The maximum is $NCLIENTS."

	read new_n

	if [[ $new_n == "" ]]
	then
		new_n=$NCLIENTS_START
	else
		if [[ $new_n -lt 0 || $nclients -gt $NCLIENTS ]]
		then
			echo "$(tput setaf 1)Invalid input. I'm exiting now. Please restart this program.$(tput sgr0)"
			exit 1
		fi
	fi
	
	if [[ $new_n -gt 95 ]]
	then
		echo "$(tput bold)$(tput setaf 1)"
		echo "Sessions with more than 95 clients generally don't work."
		echo "This is a Linux limitation. Continue at your own risk."
		echo "$(tput sgr0)"
		
		sleep 2
	fi

	echo "$(tput setaf 5)Session with $new_n clients will be started in ..."
	echo "$(tput setaf 5)Press Ctrl+C to abort."
	echo

	for item in `seq 5 -1 1`
	do
		echo -n $item"... "
		sleep 1
	done

	echo "$(tput sgr0)"

	./generate_session.py $(($new_n + 1)) > /share/.session/sessionfile
else
	echo "$(tput sgr0)"

	cp "$1" /share/.session/sessionfile

	new_n=$(( $(wc -l < /share/.session/sessionfile) - 1 ))
fi

if [[ -f /etc/zu_oem ]]; then
	sudo mount -o remount,rw,nosuid,nodev,noexec,relatime,hidepid=2 /proc
	echo 16384 | sudo tee /proc/sys/kernel/shmmni > /dev/null
fi

chmod 600 /share/.session/sessionfile

zCHANNEL=$(( $RANDOM % 1024 ))
echo $zCHANNEL > /share/.session/channel

./rebalance_cpus.sh

rm -rf /tmp/clientState
mkdir /tmp/clientState
chmod 777 /tmp/clientState

./list_clients.sh | while read line ;
do
	new_n=$(($new_n - 1))

	VDISP=$(grep -wE "$line" /share/.session/sessionfile | awk '{print $2}')
	P1=$(grep -wE "$line" /share/.session/sessionfile | awk '{print $3}')
	P2=$(grep -wE "$line" /share/.session/sessionfile | awk '{print $4}')
	VNCPW=$(grep -wE "$line" /share/.session/sessionfile | awk '{print $6}')
	
	./start_x.sh $line $VDISP
	./postx_hooks.sh $line $VDISP
	./start_vnc.sh $line $VDISP $P1 $VNCPW
	
	tmux new-session -d "../utils/noVNC/utils/launch.sh --vnc 127.0.0.1:$P1 --listen $LISTEN:$P2"

	echo -n "Started $line. "
	
	if [ $line == $PREFIX"0" ]; then
		SUBDIR=/share/scratch/$(date +%Y%m%d%H%M%S)
		sudo mkdir $SUBDIR
		sudo chown $PREFIX"0":clients $SUBDIR
		sudo chmod g+w $SUBDIR
		sudo chmod g+x $SUBDIR
		
		sudo cp /share/.session/sessionfile $SUBDIR/
		sudo chown lab:clients /share/.session/sessionfile $SUBDIR/sessionfile
		sudo chmod 600 /share/.session/sessionfile $SUBDIR/sessionfile
		
		./start_ztree.sh $line $VDISP "$(sudo cat /home/$line/.zu/core)" $SUBDIR $zCHANNEL
		echo "Started z-Tree. Waiting for five seconds."

		sleep 5
	else
		./start_zleaf.sh $line $VDISP "$(sudo cat /home/$line/.zu/core)" $FONTSIZE $zCHANNEL
		echo "Started z-Leaf."
	fi

	if [[ $new_n -lt 0 ]]
	then
		break
	fi
done

./generate_nginx.py > /share/.session/zTu2_paths.conf

if [[ -f /etc/zu_oem && $USE_THIN != 1 ]]; then
	./generate_nginx_443.py > /share/.session/nginx_443.conf

	if [[ -f ~/.zu/new_nginx_path ]]; then
		SITES_PATH="/etc/nginx/sites-enabled/zTu"
	else
		SITES_PATH="/etc/nginx/sites-available/default"
	fi

	sudo systemctl stop nginx.service && sudo cp /share/.session/nginx_443.conf $SITES_PATH && sudo cp /share/.session/zTu2_paths.conf /etc/nginx/ && sudo systemctl start nginx.service
	estatus=$?
fi

if [[ $USE_THIN == 1 ]]; then
	cp /share/.session/zTu2_paths.conf /share/.session/nginx.conf

	./thin_hooks.sh
	estatus=$?
fi

if [[ $estatus == 0 && -z "$DISPLAY" ]]; then
	./show_urls.sh
fi

if [[ $estatus != 0 ]]; then
	echo YES | ./kill_all.sh > /dev/null 2>&1

	ping -c1 -w1 "$(cat /share/wg_thin_ip)" > /dev/null
	thin_reachable=$?

	if [[ $USE_THIN == 1 && $thin_reachable == 0 ]]; then
		echo $'\e[0;30;41m'WARNING: Please inform your \"Thin\" server administrator$'\e[0m'
		echo $'\e[0;30;41m'that you use the new link format.$'\e[0m'
	fi

	echo
	echo "$(tput setaf 1)Starting the session was not successful. Please try again."
	echo "$(tput setaf 1)All clients have been terminated."

	if [ -n "$DISPLAY" ]
	then
		echo "$(tput sgr0)"
		echo "Press enter to close this window."
		read
	fi
	
	exit $estatus
fi
