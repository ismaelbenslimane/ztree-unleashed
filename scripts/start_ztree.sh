#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

# usage: ./start_ztree.sh user display core subdir channel

if [[ $# -lt 5 || $# -gt 5 ]]
then
	echo "ERROR: Invalid number of arguments to start_ztree.sh. Exiting."
	exit 1
fi

if [ -f ~/.zu/language ]; then
	ZTREEFLAGS="/language $(cat ~/.zu/language)"
else
	ZTREEFLAGS=""
fi

./as_another_tmux.sh $1 "bash -c 'cd $4; DISPLAY=:$2 /usr/bin/taskset -c $3 /usr/bin/wine /share/.session/ztree.exe $ZTREEFLAGS /channel $5'"

# source for the taskset cmd: Oliver Kirchkamp
# https://www.kirchkamp.de/lab/zTree.html
# Thanks!
