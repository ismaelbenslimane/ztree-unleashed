#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

set -euo pipefail

source read_settings.sh

echo "The following clients are available:$(tput sgr0)"
echo

OOUT=""
for i in `seq 1 $NCLIENTS`;
do
	OOUT=$OOUT" "$PREFIX$i
done

echo $OOUT | fold -s -w 60
echo
echo "$(tput setaf 6)Which z-Leaf do you want to restart?"
echo "$(tput setaf 6)If you wish to restart all, enter ALL."
echo "$(tput setaf 1)NOTE: You might lose data if you proceed."
echo "$(tput sgr0)"
read user

if [[ $user == "ALL" ]]
then
	./list_clients.sh | grep -v "^$PREFIX"0 | while read user ;
	do
		sudo pkill --signal=INT -u $user wine
		sleep 0.4
		VDISP=$(grep -wE "$user" /share/.session/sessionfile | awk '{print $2}')
		
		if [[ -n "$VDISP" ]]
		then
			./start_zleaf.sh $user $VDISP "$(sudo cat /home/$user/.zu/core)" $FONTSIZE $(cat /share/.session/channel)
			
			echo "Restarted z-Leaf on $user."
		fi
	done
else
	sudo pkill --signal=INT -u $user wine
	sleep 0.4
	VDISP=$(grep -wE "$user" /share/.session/sessionfile | awk '{print $2}')
	
	if [[ -n "$VDISP" ]]
	then
		./start_zleaf.sh $user $VDISP "$(sudo cat /home/$user/.zu/core)" $FONTSIZE $(cat /share/.session/channel)
		
		echo "Restarted z-Leaf on $user."
	fi
fi
