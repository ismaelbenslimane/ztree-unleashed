#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

if [ -z "$1" ]
then
	LEN=32
else
	LEN=$1
fi

cat /dev/urandom | tr -dc A-Za-z0-9 | head -c $LEN
