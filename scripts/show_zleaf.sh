#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

PREFIX=$(cat ~/.zu/prefix)
NCLIENTS=$(cat ~/.zu/nclients)

echo "$(tput setaf 2)z-Tree unleashed v1.0"
echo "Show z-Leaf"
echo
echo "The following clients are available:$(tput sgr0)"
echo

OOUT=""
for i in `seq 1 $NCLIENTS`;
do
	OOUT=$OOUT" "$PREFIX$i
done

echo $OOUT | fold -s -w 60
echo
echo "$(tput setaf 6)Which client do you want to view?"
echo "$(tput setaf 6)NOTE: This is VIEW-ONLY. You cannot interact"
echo "with the screen directly."
echo "$(tput sgr0)"
read user

VNCPORT=$(grep -wE $user /share/.session/sessionfile | awk '{print $3}')

cat ~/.zu/viewpasswd | vncviewer -shared -viewonly -autopass 127.0.0.1::$VNCPORT >/dev/null 2>&1
