#!/usr/bin/env python3

from os.path import expanduser

with open(expanduser("~/.zu/zuserver"), 'r') as fp:
	zuserver = fp.read().strip()

with open("templates/nginx.conf", 'r') as fp:
	t_nginx = fp.read()

nginx_out = ''

with open("/share/.session/sessionfile", 'r') as fp:
	for line in fp:
		credentials = line.strip().split('\t')
		
		nginx_out = nginx_out + t_nginx.replace('ENTRYPOINT', credentials[4]).replace('COMMENT', credentials[0]).replace('ZUSERVER', zuserver).replace('NOVNCPORT', credentials[3]).replace('VNCPW', credentials[5])

print(nginx_out)
