#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

set -o pipefail

source read_settings.sh

CURRENT_RUN=$(wc -l < /share/.session/sessionfile)
MAX_CLIENTS_ADD=$((95-$CURRENT_RUN))

if sudo pgrep Xvfb > /dev/null; then	
	echo "$(tput setaf 6)How many clients do you wish to add?"
	echo "$(tput setaf 6)Press enter to add none."
	echo "$(tput setaf 6)The maximum you can add are $MAX_CLIENTS_ADD."
	echo "$(tput setaf 1)PLEASE NOTE: ztree distributions older than 5.0.16 do not"
	echo "$(tput setaf 1)support adding clients if a treatment has already been run."
	
	read add_new

	TOT_CLIENTS=$(($add_new+$CURRENT_RUN))

	if [[ $add_new == "" ]]
	then
		echo "$(tput setaf 1)You decided to add no new clients. I'm exiting now."
		exit 1
	elif [[ $add_new -lt 0 ]]
	then
		echo "$(tput setaf 1)Invalid input. I'm exiting now."
		exit 1
	elif [[ $add_new -gt $MAX_CLIENTS_ADD ]]
	then
		echo "$(tput setaf 1)You tried to add to many clients. I'm exiting now."
		echo "$(tput setaf 1)Please restart this program.$(tput sgr0)"
		exit 1
	fi
else
	echo "$(tput setaf 1)"
	echo "It seems like you are not running a session yet. No"
	echo "clients can be added. Please \"Start Session\" first."
	echo "$(tput sgr0)"

	if [ -n "$DISPLAY" ]
	then
		echo "Press enter to close this window."
		read
	fi

	exit 1
fi

echo "$(tput setaf 5)Amending your current session."
echo "$(tput setaf 5)There will be a total of $(($TOT_CLIENTS-1)) clients."
echo "$(tput setaf 5)$add_new new clients will be added in ..."
echo "$(tput setaf 5)Press Ctrl+C to abort."
echo

for item in `seq 5 -1 1`
do
	echo -n $item"... "
	sleep 1
done

echo "$(tput sgr0)"

cp /share/.session/sessionfile{,2}
./generate_session.py $(($add_new)) | tee /share/.session/extension >> /share/.session/sessionfile2

j=$(($CURRENT_RUN-1))
zCHANNEL=$(cat /share/.session/channel)

awk '{print $1}' /share/.session/extension | while read line ;
do
	add_new=$(($add_new-1))
	j=$(($j+1))

	VDISP=$(grep -wE "$line" /share/.session/extension | awk '{print $2}')
	P1=$(grep -wE "$line" /share/.session/extension | awk '{print $3}')
	P2=$(grep -wE "$line" /share/.session/extension | awk '{print $4}')
	VNCPW=$(grep -wE "$line" /share/.session/extension | awk '{print $6}')

	./start_x.sh $PREFIX$j $VDISP
	./postx_hooks.sh $PREFIX$j $VDISP
	./start_vnc.sh $PREFIX$j $VDISP $P1 $VNCPW

	tmux new-session -d "../utils/noVNC/utils/launch.sh --vnc 127.0.0.1:$P1 --listen $LISTEN:$P2"

	echo -n "Started $PREFIX$j. "

	./start_zleaf.sh $PREFIX$j $VDISP "$(sudo cat /home/$line/.zu/core)" $FONTSIZE $zCHANNEL
	echo "Started z-Leaf."

	if [[ $add_new -lt 0 ]]
	then
		break
	fi
done

./canonicalize_sessionfile.py > /share/.session/sessionfile

./generate_nginx_443.py > /share/.session/nginx_443.conf
./generate_nginx.py > /share/.session/zTu2_paths.conf

if [[ -f /etc/zu_oem && $USE_THIN != 1 ]]; then
	if [[ -f ~/.zu/new_nginx_path ]]; then
		SITES_PATH="/etc/nginx/sites-enabled/zTu"
	else
		SITES_PATH="/etc/nginx/sites-available/default"
	fi

	sudo cp /share/.session/nginx_443.conf $SITES_PATH && sudo cp /share/.session/zTu2_paths.conf /etc/nginx/ && sudo systemctl reload nginx.service
	estatus=$?
fi

if [[ $USE_THIN == 1 ]]; then
	cp /share/.session/zTu2_paths.conf /share/.session/nginx.conf

	./thin_hooks.sh
	estatus=$?
fi

if [[ $estatus == 0 ]]; then
	./show_urls.sh
fi

if [[ $estatus != 0 ]]; then
	ping -c1 -w1 "$(cat /share/wg_thin_ip)" > /dev/null
	thin_reachable=$?

	if [[ $USE_THIN == 1 && $thin_reachable == 0 ]]; then
		echo $'\e[0;30;41m'WARNING: Please inform your \"Thin\" server administrator$'\e[0m'
		echo $'\e[0;30;41m'that you use the new link format.$'\e[0m'
	fi

	echo
	echo "$(tput setaf 1)Extending the session was not successful. Your session may"
	echo "$(tput setaf 1)be in an inconsistent state. Please terminate the session"
	echo "$(tput setaf 1)and use \"Reuse sessionfile\" to try again."
fi

rm /share/.session/{sessionfile2,extension}
