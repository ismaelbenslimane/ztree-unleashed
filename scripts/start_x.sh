#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

# usage: ./start_x.sh user display

./as_another_tmux.sh $1 "Xvfb :$2 -screen 0 $(cat /share/geometry)x16 -ac"
sleep 0.2
