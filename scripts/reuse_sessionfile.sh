#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

set -euo pipefail

NFILES=$(find /share/scratch/??????????????/sessionfile 2>/dev/null | wc -l)

if [ $NFILES -gt 0 ]; then
	echo "$(tput setaf 3)The following sessionfiles were found:$(tput sgr0)"
	echo
	find /share/scratch/??????????????/sessionfile | cat -n
	echo
	while :
	do
		echo "$(tput setaf 5)Please enter your selection [1-$NFILES]:$(tput sgr0)"
		read selection

		if [[ $selection -lt 1 || $selection -gt $NFILES ]]
		then
			echo "$(tput setaf 1)Invalid input. Enter a natural number between 1 and $NFILES.$(tput sgr0)"
		else
			break
		fi
	done

	sfile=$(find /share/scratch/??????????????/sessionfile | head -n $selection | tail -n 1)
	
	lines=$(wc -l < "$sfile")

	echo "$(tput setaf 5)The following sessionfile will be reused:"
	echo
	echo "$(tput setaf 3)$sfile$(tput sgr0)"
	echo
	head -n2 "$sfile"
	echo "$(tput setaf 5)... ($(( $lines - 2 >= 0 ? $lines - 2 : 0 )) lines omitted) ...$(tput sgr0)"
	tail -n2 "$sfile"
	echo
	echo "$(tput setaf 1)Press enter to continue, or Ctrl+C to abort."
	echo
	read

	./start_session_simple.sh "$sfile"
else
	SUBDIR="/share/scratch/$(date +%Y%m%d%H%M%S)"
	sudo mkdir $SUBDIR
	sudo chown $PREFIX"0":clients $SUBDIR
	sudo chmod g+w $SUBDIR
	sudo chmod g+x $SUBDIR
	
	echo "$(tput setaf 1)No sessionfiles were found. If you want to reuse"
	echo "$(tput setaf 1)a private sessionfile, you can upload it to"
	echo "$(tput setaf 6)$SUBDIR"
	echo "$(tput setaf 1)and it will appear during the next run of this"
	echo "$(tput setaf 1)program (as long as it's called 'sessionfile')."
	exit 1
fi
