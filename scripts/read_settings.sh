#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

mkdir -p ~/.zu
chmod -R 700 ~/.zu

# defaults

DEFAULT_SSL_CERT="/etc/ssl/certs/ssl-cert-snakeoil.pem"
DEFAULT_SSL_KEY="/etc/ssl/private/ssl-cert-snakeoil.key"
DEFAULT_PREFIX="client"
DEFAULT_HOST="127.0.0.1"
DEFAULT_LISTEN="127.0.0.1"
DEFAULT_ZUSERVER="127.0.0.1"
DEFAULT_GEOMETRY="1024x768"
DEFAULT_USE_THIN=0
DEFAULT_NCLIENTS=95
DEFAULT_FONTSIZE=12
DEFAULT_ZTREELANG="english"

if [ -f ~/.zu/ssl_cert ]; then
	SSL_CERT=$(cat ~/.zu/ssl_cert)
else
	SSL_CERT=$DEFAULT_SSL_CERT
fi

if [ -f ~/.zu/ssl_key ]; then
	SSL_KEY=$(cat ~/.zu/ssl_key)
else
	SSL_KEY=$DEFAULT_SSL_KEY
fi

if [ -f ~/.zu/prefix ]; then
	PREFIX=$(cat ~/.zu/prefix)
else
	PREFIX=$DEFAULT_PREFIX
fi

if [ -f ~/.zu/host ]; then
	HOST=$(cat ~/.zu/host)
else
	HOST=$DEFAULT_HOST
fi

if [ -f ~/.zu/listen ]; then
	LISTEN=$(cat ~/.zu/listen)
else
	LISTEN=$DEFAULT_LISTEN
fi

if [ -f ~/.zu/zuserver ]; then
	ZUSERVER=$(cat ~/.zu/zuserver)
else
	ZUSERVER=$DEFAULT_ZUSERVER
fi

if [ -f /share/geometry ]; then
	GEOMETRY=$(cat /share/geometry)
else
	GEOMETRY=$DEFAULT_GEOMETRY
fi

if [ -f ~/.zu/use_thin ]; then
	USE_THIN=$(cat ~/.zu/use_thin)
else
	USE_THIN=$DEFAULT_USE_THIN
fi

if [ -f ~/.zu/nclients ]; then
	NCLIENTS=$(cat ~/.zu/nclients)
else
	NCLIENTS=$DEFAULT_NCLIENTS
fi

if [ -f ~/.zu/nclients_start ]; then
	NCLIENTS_START=$(cat ~/.zu/nclients_start)
else
	NCLIENTS_START=$NCLIENTS
fi

if [ -f ~/.zu/fontsize ]; then
	FONTSIZE=$(cat ~/.zu/fontsize)
else
	FONTSIZE=$DEFAULT_FONTSIZE
fi

if [ -f ~/.zu/language ]; then
	ZTREELANG=$(cat ~/.zu/language)
else
	ZTREELANG=$DEFAULT_ZTREELANG
fi
