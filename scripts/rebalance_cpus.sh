#!/usr/bin/env bash

source read_settings.sh

NPROC=$(nproc --all)

for i in `seq 0 $NCLIENTS`;
do
	if [[ $NPROC -gt 3 ]]
	then
		if [[ $i -eq 0 ]]
		then
			echo $(($NPROC-1)) | sudo tee "/home/$PREFIX$i/.zu/core" > /dev/null
		else
			echo $(( $i % ($NPROC-1) )) | sudo tee "/home/$PREFIX$i/.zu/core" > /dev/null
		fi
	else
		echo $(( $i % $NPROC )) | sudo tee "/home/$PREFIX$i/.zu/core" > /dev/null
	fi
	
done
