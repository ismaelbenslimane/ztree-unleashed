#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

# usage: ./postx_hooks.sh user display

if [[ -f start_wm.sh ]]; then
	./start_wm.sh $1 $2
fi
