#!/usr/bin/env python3

import sys
import random
import string
import socket, errno

from os.path import expanduser

def genpw(len = 16, letters = string.ascii_letters + string.digits):
	return ''.join(random.choices(letters, k = len))

with open(expanduser("~/.zu/prefix"), 'r') as fp:
	prefix = fp.read().strip()

n_clients = int(sys.argv[1])
users = [prefix + str(_) for _ in range(n_clients)]

displays = random.sample(range(10, 1000000), k = n_clients)
vncpw = [genpw(3) for _ in range(n_clients)]

while True:
	ports1 = random.sample(range(49152, 65536), k = n_clients) # local VNC port
	ports2 = random.sample(range(49152, 65536), k = n_clients) # noVNC port (exposed to reverse proxy)

	entryp = [genpw(8) for _ in range(n_clients)] # this is not a port anymore :-) don't change it

	cont = False

	for ports in [ports1, ports2]:
		for port in ports:
			if port in ports1 and port in ports2:
				cont = True
			else:
				sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

				try:
					sock.bind(("127.0.0.1", port))
					sock.close()
				except:
					cont = True

	if cont is False:
		break

for z in zip(users, displays, ports1, ports2, entryp, vncpw):
	print(f'{z[0]}\t{z[1]}\t{z[2]}\t{z[3]}\t{z[4]}\t{z[5]}')

with open(expanduser("~/.zu/viewpasswd"), 'w') as fp:
	fp.write(genpw(8))
