#!/usr/bin/env python3

import sys
import random
import string
import socket, errno

from os.path import expanduser

with open(expanduser("~/.zu/prefix"), 'r') as fp:
	prefix = fp.read().strip()

with open('/share/.session/sessionfile2', 'r') as fp:
	for j, line in enumerate(fp):
		credentials = line.strip().split('\t')
		print(f'{prefix + str(j)}\t{credentials[1]}\t{credentials[2]}\t{credentials[3]}\t{credentials[4]}\t{credentials[5]}')
